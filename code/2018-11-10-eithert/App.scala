object App {
// Pre-blog setup
val random = new scala.util.Random()
def main(args: Array[String]): Unit = {
// Blog starts here

// We need to import the Future class,
// and bring an execution context into implicit scope.
import scala.concurrent.Future
implicit val ec = scala.concurrent.ExecutionContext.global

// We will be modelling a bunch of functions which return something of type
// Future[Either[Error, A]]. I don't want to keep writing it so I'll use a 
// type alias with a very broad Error trait, leaving A generic.
trait Error
type Result[A] = Future[Either[Error, A]]

def init: Result[String] = slowAndUncertain("init")

// For convenience, a method to wait for our futures to finish.
// I don't care how long it takes.
import scala.concurrent.Await
import scala.concurrent.duration.Duration
def await[A](f: Future[A]) = Await.result(f, Duration.Inf): A

println("A Basic Example")
println("Wait for a result to arrive, and print it:")
println(await(init))

// A More Complex Example

// Define data types
case class UserId(s: String)
case class AuthenticatedUser(id: UserId)
case class UserLocation(user: AuthenticatedUser)
case class Pub(name: String)

//  Mock up service calls (implementations not included in the blog post).
def askForId: Result[UserId] = slowDownAndMakeUncertain[Unit, UserId](
  _ => GetIDError,
  _ => UserId("ben"),
  ())
def authenticate(userId: UserId): Result[AuthenticatedUser] = slowDownAndMakeUncertain[UserId, AuthenticatedUser](
  u => InvalidUser(s"Could not authenticate user [$u]."),
  AuthenticatedUser,
  userId)
def getLocation(user: AuthenticatedUser): Result[UserLocation] = slowDownAndMakeUncertain[AuthenticatedUser, UserLocation](
  u => LocationUnknown(s"Could not locate user [$u]."),
  UserLocation,
  user)
def findPubs(userLocation: UserLocation): Result[List[Pub]] = slowDownAndMakeUncertain[UserLocation, List[Pub]](
  l => InvalidLocation(s"Could not find pubs for location [$l]."),
  _ => List(Pub("The Duke's Monad")),
  userLocation)

// Get the list of pubs, by flatMapping each result in succession
def pubs: Result[List[Pub]] = askForId.flatMap {
  case Left(e) => Future.successful(Left(e))
  case Right(r) => authenticate(r).flatMap {
    case Left(e) => Future.successful(Left(e))
    case Right(r) => getLocation(r).flatMap {
      case Left(e) => Future.successful(Left(e))
      case Right(r) => findPubs(r)
    }
  }
}

// Note on flatMap syntax, the following are equivalent:
def shorter = askForId.flatMap { case _ => ??? }
def longer  = askForId.flatMap(result => result match { case _ => ??? })

println()
println("A More Complex Example")
println(await(pubs))

// Let's refactor to make this more manageable.

// Curried funtion, which we can provide an onSuccess function,
// which will return a function to use inside our flatMap calls.
def handle[A,B](onSuccess: A => Result[B])(in: Either[Error, A]): Result[B] = in match {
  case Left(e) => Future.successful(Left(e))
  case Right(x) => onSuccess(x)
}

// This allows to write the entire chain as nested flatMap calls
def betterPubs: Result[List[Pub]] = askForId
  .flatMap(handle(authenticate(_)
    .flatMap(handle(getLocation(_)
      .flatMap(handle(findPubs))))))

println()
println("With Cleaner Code.")
println(await(betterPubs))

// With a for comprehension
def evenBetterPubs: Result[List[Pub]] = for {
  id       <- askForId
  user     <- handle(authenticate)(id)
  location <- handle(getLocation)(user)
  pubs     <- handle(findPubs)(location)
} yield pubs

println()
println("With a For-Comprehension.")
println(await(evenBetterPubs))

// Now let's do it with EitherT
import cats.data.EitherT

// I'll use another type alias to save space. Sorry about the name.
type ResulT[A] = EitherT[Future, Error, A]

// It's trivial to transform our old, Future[Either] methods to return EitherT[Future].
def askForIdT(): ResulT[UserId] =  EitherT(askForId)
def authenticateT(userId: UserId): ResulT[AuthenticatedUser] = EitherT(authenticate(userId))
def getLocationT(user: AuthenticatedUser): ResulT[UserLocation] = EitherT(getLocation(user))
def findPubsT(userLocation: UserLocation): ResulT[List[Pub]] =  EitherT(findPubs(userLocation))

// We need an instance of the Monad typeclass for Future.
import cats.instances.future._

def bestPubs: ResulT[List[Pub]] = for {
  id       <- askForIdT
  user     <- authenticateT(id)
  location <- getLocationT(user)
  pubs     <- findPubsT(location)
} yield pubs

println()
println("Using EitherT.")
println(await(bestPubs.value))

// Done, for now...

// Un-bloggables

// Slowly adds an excalamation mark to a string. Can fail. Totally pointless.
def slowAndUncertain(s: String): Result[String] = slowDownAndMakeUncertain[String,String](_ => ServerDown, _ + "!", s)

// Bunch of errors for printing
case object ServerDown extends Error
case object GetIDError extends Error
case class InvalidUser(msg: String) extends Error
case class LocationUnknown(msg: String) extends Error
case class InvalidLocation(msg: String) extends Error

// Emulate a call to the other team's service.
// Takes an error handling function and a regular function from A to B, slows it down, and gives it a chance to fail.
def slowDownAndMakeUncertain[A, B](onFailure: A => Error, onSuccess: A => B, input: A): Result[B] = Future {
  println(s"Processing [$input].")
  // Slow it down
  Thread.sleep(random.nextInt(1000))
  // Randomly fail
  if(random.nextInt(10) > 0) {
    val result = onSuccess(input)
    println(s"Success: $result.")
    Right(result)
  } else {
    val error = onFailure(input)
    println(s"Failure: $error.")
    Left(error)
  }
}


}
}