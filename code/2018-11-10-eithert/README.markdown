Demo application for [this blog post](https://benmosheron.gitlab.io/blog/2018/11/20/eithert.html). 

Pre-requisites: install scala and SBT.

To run, clone this repo (or just copy the App.scala and build.sbt), then use `sbt run` from the directory containing those two files.