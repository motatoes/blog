Prerequisites: Install .Net Core CLI

Build and run via dotnet (slow)
`dotnet run`

Just run (fast)
`dotnet bin/Debug/netcoreapp2.1/Functions.dll`

Build executable for a platform
`dotnet publish -c release -r osx-x64`
`dotnet publish -c release -r win-x64`
`dotnet publish -c release -r linux-x64`