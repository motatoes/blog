package com.benmosheron.scalaforjava

object ScalaApp {

  import com.benmosheron.runtime.RuntimeInterface

  class JustChecking extends RuntimeInterface {
    def getWord(): String = "one"
    def isLoaded(): Boolean = true
  }

  def main(args: Array[String]) = {
    println(s"The word is [${new JustChecking().getWord()}].")
  }
}
