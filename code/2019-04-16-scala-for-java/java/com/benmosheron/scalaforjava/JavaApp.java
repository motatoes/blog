package com.benmosheron.scalaforjava;

import com.benmosheron.printing.Printer;
import com.benmosheron.runtime.RuntimeInterface;

import java.util.Optional;

class JavaApp {

  private static final String RUNTIME_IMPL 
    = "com.benmosheron.runtime.RuntimeImplementation";

  public static void main(String[] args){
    Printer printer = new Printer("");
    Printer bulletPointPrinter =  new Printer("  - ");
    printer.println("Neat:");
    bulletPointPrinter.println("Really enjoying this");
    bulletPointPrinter.println("Thanks for reading");
    printer.println("Trying to load runtime dependency:");
    loadRuntimeDependency(bulletPointPrinter);
  }

  private static void loadRuntimeDependency(Printer printer){
    ClassLoader loader = JavaApp.class.getClassLoader();

    loadRuntimeImplementation(loader, printer)
      .flatMap(c -> instantiateImplementation(c, printer))
      .filter(RuntimeInterface::isLoaded)
      .ifPresent(ri -> printer.println("The word is [" + ri.getWord() + "]."));
  }

  private static Optional<Class<?>> loadRuntimeImplementation(ClassLoader loader, Printer printer){
    // Load the RuntimeImplementation class using the class loader.
    printer.println("Loading runtime dependency...");
    try {
      Class<?> c = loader.loadClass(RUNTIME_IMPL);
      printer.println("Success!");
      return Optional.of(c);
    } catch(ClassNotFoundException ex){
      printer.println("Failed! ClassNotFoundException.");
      return Optional.empty();
    }
  }

  private static Optional<RuntimeInterface> instantiateImplementation(Class<?> runtimeClass, Printer printer){
    printer.println("Instantiating RuntimeImplementation...");
    try {
      RuntimeInterface ri = (RuntimeInterface)runtimeClass.getDeclaredConstructor().newInstance();
      printer.println("Success!");
      return Optional.of(ri);
    }
    catch (Exception ex){
      printer.println("Failed! " + ex.getMessage());
      return Optional.empty();
    }
  }

}