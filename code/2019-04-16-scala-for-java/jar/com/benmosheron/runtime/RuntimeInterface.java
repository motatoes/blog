package com.benmosheron.runtime;

public interface RuntimeInterface {
  boolean isLoaded();
  String getWord();
}