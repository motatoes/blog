This directory (`jar`) is for building the compile and runtime dependency jar files used by JavaApp.

Compile from this directory with:

`javac -d out com/benmosheron/runtime/RuntimeInterface.java com/benmosheron/runtime/RuntimeImplementation.java`

After compiling, we build two jar files (again, run from this directory) with:

`jar cvf Compile.jar -C out com/benmosheron/runtime/RuntimeInterface.class`
`jar cvf Runtime.jar -C out com/benmosheron/runtime/RuntimeImplementation.class`

This will create our compile-time dependency `Compile.jar` and our runtime dependency `Runtime.jar` in this directory.

Copy the jar files to the `java` project's `lib` folder (yet again, run from this directory):

`cp Compile.jar ../java/lib/compile`
`cp Runtime.jar ../java/lib/runtime`
