// Script for running sorting algorithms on canvases.

function randFloat(min, max){
  return Math.random() * (max - min) + min;
}

function randInt(minInc,maxInc){
  return Math.floor(randFloat(minInc, maxInc + 1));
}


const rand255 = () => randInt(0,255)
const col = (r, g, b) => `rgb(${r}, ${g}, ${b})`
const LINE_WIDTH = 5

function render_(ctx, things){
  things.forEach(t => t.draw(ctx))
}

function render(canvas, ctx, things){
  ctx.clearRect(0, 0, canvas.width, canvas.height)
  render_(ctx, things)
}

/////////
// HSL //
/////////

// Transform [0:256] onto [0:1]
function trans255to1(x){ return x/255 }

// Calculate the hue
function hue(values, min, max){
  const r = values[0]
  const g = values[1]
  const b = values[2]
  let h = 0
  // Hue is zero if there is no colour variation (grey!)
  if(min.v === max.v){ return h }
  switch (max.name) {
    case "r":
      h = 60 * (0 + (g.v - b.v)/(max.v - min.v))
      break;
    case "g":
      h = 60 * (2 + (b.v - r.v)/(max.v - min.v))
      break;
    case "b":
      h = 60 * (4 + (r.v - g.v)/(max.v - min.v))
      break;
  }
  if(h < 0){ h = h + 360 }
  return h
}

function saturation(min, max){
  let s = 0
  // Zero saturation for pure white or pure black
  if(max.v === 0 || min.v === 1) { return s }
  return (max.v - min.v)/(1 - Math.abs(max.v + min.v - 1))
}

function lightness(min, max){ return (min.v + max.v)/2 }

function hsl(rgb){
  const names = ["r","g","b"]
  // Collect values with their names, original value, and value mapped onto [0:1]
  const values = rgb
    .map(function(v,i){ return { name: names[i], orig: v, v: trans255to1(v)} })
  
  // Get the max and min
  const max = values
    .reduce(function(next,acc){ 
      if(next.v > acc.v){
        return next
      } else {
        return acc
      }
    })
  const min = values
    .reduce(function(next,acc){ 
      if(next.v < acc.v){
        return next
      } else {
        return acc
      }
    })

  // Calculate the hue, saturation and lightness
  return {
    h: hue(values, min, max),
    s: saturation(min, max),
    l: lightness(min, max)
  }
}

//////////////////////////////////
// Functions for Creating Lines //
//////////////////////////////////

function mkLine(x,r,g,b,f){
  const c = col(r,g,b)
  return {
    x: x,
    marked: false,
    value: f(r,g,b),
    draw: function(ctx){
      ctx.fillStyle = c
      ctx.fillRect(this.x,0,LINE_WIDTH,90)
      if(this.marked){
        ctx.fillRect(this.x,95,LINE_WIDTH,5)
      }
    }
  }
}

function debugLine(x){
  const vals = [205,86,66,236]
  const i = (x/LINE_WIDTH)%vals.length
  const r = vals[i]
  return mkLine(x,r,r,r,() => r)
}

function greyscaleLine(x){
  const r = rand255()
  return mkLine(x,r,r,r,() => r)
}

function greyscaleOrdLine(x){
  const r = (255/100) * x / LINE_WIDTH
  return mkLine(x,r,r,r,() => r)
}

function colourfulLine(x){
  const v = (x%250 / 125)
  let v2 = null
  if(v <= 1){v2 = v} else { v2 = (1 - (v - 1)) }
  const r = (x / 500) * 200 + 50
  const g = v2 * 200 + 50
  const b = (1 - (x / 500)) * 200 + 50
  return mkLine(x,r,g,b,() => g)
}

function vibrantLine(x, f){
  if(typeof(f) === "undefined") f = (r_,g_,b_) => (r_+g_+b_)/3
  const r = rand255()
  const g = rand255()
  const b = rand255()
  return mkLine(x,r,g,b,f)
}

function mkHslLine(x,h,s,l,f){
  return {
    x: x,
    marked: false,
    value: f(h,s,l),
    draw: function(ctx){
      ctx.fillStyle = `hsl(${h},${s*100}%,${l*100}%)`
      ctx.fillRect(this.x,0,LINE_WIDTH,90)
      if(this.marked){
        ctx.fillRect(this.x,95,LINE_WIDTH,5)
      }
    }
  }
}

function hslLine(x, f){
  const h = randInt(0,359)
  const s = randFloat(0,1)
  const l = randFloat(0,1)
  return mkHslLine(x,h,s,l,f)
}

function hLine(x, f){
  const h = randInt(0,359)
  let s = 0.5
  let l = 0.5
  return {
    x: x,
    marked: false,
    value: f(h,s,l),
    draw: function(ctx){
      s = sliderVals["saturationSlider"] / 100
      l = sliderVals["lightnessSlider"] / 100
      ctx.fillStyle = `hsl(${h},${s*100}%,${l*100}%)`
      ctx.fillRect(this.x,0,LINE_WIDTH,90)
      if(this.marked){
        ctx.fillRect(this.x,95,LINE_WIDTH,5)
      }
    }
  }
}

function hLimitLine(x, f){
  const h = randInt(0,359)
  const s = randFloat(0.5,1)
  const l = randFloat(0.2,0.8)
  return mkHslLine(x,h,s,l,f)
}

// Cheat by generating a rainbow up front, then shuffling it so it looks random.
const cheatLines = range(100).map((i) => rainbowLine(i*LINE_WIDTH))
const bonusLines = range(100).map((i) => bonusify(i,rainbowLine(i*LINE_WIDTH)))
// Luckily I know how to shuffle now
function shuffleCheat(lines){
  for (let i = 0; i < lines.length; i++) {
    const j = randInt(i, lines.length - 1)
    const mem = lines[i]
    lines[i] = lines[j]
    lines[j] = mem
    lines[i].x = i * LINE_WIDTH
  }
}
shuffleCheat(cheatLines)
shuffleCheat(bonusLines)

function cheatRainbowLine(x){
  return cheatLines[x / LINE_WIDTH]
}

function bonusify(i, line){
  let bonus = []
  if(i === 48) bonus = [45]
  if(i === 49) bonus = [35,50]
  if(i === 50) bonus = [50]
  if(i === 51) bonus = [50]
  if(i === 52) bonus = [35,50]
  if(i === 53) bonus = [45]
  if(bonus.length !== 0){
    line.draw2 = line.draw
    line.draw = function(ctx){
      this.draw2(ctx)
      ctx.fillStyle = "rgb(0,0,0)"
      bonus.forEach(y => ctx.fillRect(line.x,y,5,5))
    }
  }
  return line
}

function bonusRainbowLine(x){
  return bonusLines[x / LINE_WIDTH]
}

function rainbowLine(x){
  const h = ((x/LINE_WIDTH)/100)*360
  const s = 0.6
  const l = 0.5
  const f = () => h
  return mkHslLine(x,h,s,l,f)
}

function vibrantHalfRedLine(x){
  const r = rand255()
  const g = rand255()
  const b = rand255()
  const c = col(r,g,b)
  return {
    x: x,
    marked: false,
    value: r,
    draw: function(ctx){
      ctx.fillStyle = c
      ctx.fillRect(this.x,0,LINE_WIDTH,90)
      if(this.marked){
        ctx.fillRect(this.x,95,LINE_WIDTH,5)
      }
      ctx.fillStyle = `rgb(${r},0,0)`
      ctx.fillRect(this.x,45,LINE_WIDTH,45)
    }
  }
}

function leastGreyLine(x){
  const r = rand255()
  const g = rand255()
  const b = rand255()
  const m = (r+g+b)/3
  const sq = x => x * x
  return mkLine(x,r,g,b,() => Math.sqrt((sq(m-r) + sq(m-g) + sq(m-b))/3))
}

function range(n){
  const arr = new Array(n)
  for (let i = 0; i < n; i++) {
    arr[i] = i
  }
  return arr
}

/////////////
// Shuffle //
/////////////

function swapAnimation(lines,i,j){
  // Swaps lines i and j.
  // Done once the given number of frames have been run
  const nFrames = 30
  const init1 = lines[i].x
  const init2 = lines[j].x
  const goal1 = lines[j].x
  const goal2 = lines[i].x
  const d1 = (goal1 - init1) / nFrames
  const d2 = (goal2 - init2) / nFrames
  let frameCount = 0
  return {
    activeIndices: [i,j],
    done: () => nFrames === frameCount,
    run: function(){
      if(this.done()){return}
      lines[i].x = lines[i].x + d1
      lines[j].x = lines[j].x + d2
      frameCount++
      if(this.done()){
        lines[i].x = goal1
        lines[j].x = goal2
        const mem = lines[j]
        lines[j] = lines[i]
        lines[i] = mem
      }
    }
  }
}

function shuffleAlgo(lines, i){
  const n = lines.length
  // Swap with a random line between i and the last line
  const j = randInt(i, n - 1)
  // Exit condition is that i == n - 2
  const hasNext = () => i < n - 2
  // Next animation becomes current
  let currentAnimation = null
  if(hasNext()){
    currentAnimation = swapAnimation(lines, i, j)
  }

  return {
    hasNext: hasNext,
    getNext: () => shuffleAlgo(lines, i + 1),
    activeIndices: [i,j],
    done: () => currentAnimation == null || currentAnimation.done(),
    run: () => currentAnimation.run()
  }
}

////////////////
// Basic Sort //
////////////////

function compareLeftAnimation(lines, i){
  // This animation defers to two smaller animations.
  // First, we search all lines to the left of i until we find one (j) with a higher value or reach the end.
  // Then, we move line i to identified spot, and shift all those between the new position and i one to the right.
  // The animation is done once both are finished.
  const leftSearch = leftSearchAnimation(lines, i)
  const moveLeft = moveLeftAnimation(lines, i)
  
  let j = null
  return {
    activeIndices: [i],
    done: () => leftSearch.done() && moveLeft.done(),
    run: function(){
      if(!leftSearch.done()){
        j = leftSearch.run()
      } else if(!moveLeft.done()){
        moveLeft.run(j)
      }
    }
  }
}

function leftSearchAnimation(lines, i){
  // Search lines left of i, until a line is found with a higher value (0 if none are higher).
  // Return the found index as j. This animation is finished when j has a value.
  const unmarkAll = () => lines.forEach(l => l.marked = false)
  // Linger on each comparison for [compareFrames].
  const compareFrames = 2
  // Will be set when a match is found.
  let j = null
  // Track the number of frames spent comparing each line.
  let compareFrames_ = compareFrames
  // Track the line currently being compared to i.
  let c = i - 1
  return {
    done: () => j != null,
    run: function(){
      unmarkAll()
      if(this.done()){
        return j
      }

      // Mark i and c, to visibly show they are being compared.
      lines[i].marked = true
      lines[c].marked = true

      if(compareFrames_ > 0){
        // Linger on the comparison so we can see it happen.
        compareFrames_--
        return j
      }
      // Reset for the next comparison.
      compareFrames_ = compareFrames

      // If lines[i] is greater than or equal to lines[c], then we insert i to it's right
      if(lines[i].value >= lines[c].value){
        j = c + 1
        return j
      }

      // If we get all the way to the end, then i is the new lowest value
      if(c === 0){
        j = 0
        return j
      }

      c--
    }
  }
}

function moveLeftAnimation(lines, i){
  // Move line i to j's position.
  // move all lines between j and i right one line's width.
  // The swap animation lasts [swapFrames] frames.
  const swapFrames = 20
  let swapFrames_ = swapFrames
  return {
    done: () => swapFrames_ === 0,
    run: function(j){
      if(this.done()){ return }
      // Each frame, i moves dLeft to the left
      const dLeft = (i - j) * LINE_WIDTH / swapFrames
      // And [j:i] move dRight frames to the right
      let dRight = LINE_WIDTH / swapFrames
      // Account for no movement if i === j
      if(i === j) { dRight = 0 }
      // Move the lines
      lines[i].x -= dLeft
      const moveRight = lines.filter((l,ind) => ind >= j && ind < i)
      moveRight.map(l => l.x += dRight)
      swapFrames_--

      // And if we are done, swap the lines' indices in memory
      if(this.done()){
        const mem = lines[i]
        for (let k = i; k > j; k--) {
          lines[k] = lines[k - 1]
        }
        lines[j] = mem
      }
    }
  }
}

function basicSortAlgo(lines, i){
  const n = lines.length
  // i goes from 1 to n - 1
  const hasNext = () => i < n
  // Next animation becomes current
  let currentAnimation = null
  if(hasNext()){
    currentAnimation = compareLeftAnimation(lines, i)
  }
  return {
    hasNext: hasNext,
    getNext: () => basicSortAlgo(lines, i + 1),
    activeIndices: [i],
    done: () => currentAnimation == null || currentAnimation.done(),
    run: () => currentAnimation.run()
  }
}

/////////////////
// Bubble Sort //
/////////////////

function bubbleSwap(lines, i){
  // Swap lines i and i+1 if values need swapping
  const doSwap = lines[i].value > lines[i+1].value
  let swapFrames = null

  if(doSwap){
    swapFrames = 1
  } else {
    swapFrames = 1
  }

  const d = doSwap * LINE_WIDTH / swapFrames
  let swapFrames_ = swapFrames

  return {
    done: () => swapFrames_ == 0,
    run: function(){
      lines[i].x += d
      lines[i + 1].x -= d
      swapFrames_--

      // If done, swap the lines in memory
      if(doSwap && this.done()){
        const mem = lines[i]
        lines[i] = lines[i+1]
        lines[i+1] = mem
      }
    }
  }
}

function bubblePassAnimation(lines){
  // Make a single bubble sort pass.
  // Run should return true if sorted, or false otherwise
  const n = lines.length
  let i = 0
  let currentSwap = bubbleSwap(lines, i)
  
  function isSorted(lines_){
    let sorted = true
    for (let i_ = 1; i_ < lines_.length; i_++) {
      if(lines_[i_].value < lines_[i_ - 1].value) sorted = false
    }
    return sorted
  }

  return {
    done: () => i === n - 1,
    run: function(){
      lines.forEach(l => l.marked = false)
      lines[i].marked = true
      lines[i+1].marked = true

      // Run a frame of the current swap
      currentSwap.run()

      if(currentSwap.done()){
        i++
        if(i!== n - 1){
          currentSwap = bubbleSwap(lines, i)
        }
      }

      if(this.done() && isSorted(lines)){
        return true
      } else {
        return false
      }
    }
  }
}

function bubbleSortAlgo(lines){

  let currentAnimation = bubblePassAnimation(lines)
  let sorted = false

  return {
    hasNext: () => !sorted,
    getNext: () => bubbleSortAlgo(lines),
    activeIndices: [],
    done: () => currentAnimation.done(),
    run: () => {
      if(sorted) return;
      sorted = currentAnimation.run()
    }
  }
}

////////////////
// Merge Sort //
////////////////

function generateMergeSortAnimations(X){
  if(X.length === 0){ throw `Can't sort ${X}.` }
  // Sort the values in X, where X is an array of value, index pairs
  // e.g. [{i: 0, value: 20},{i: 1, value: 100},{i: 2, value: 30}]

  // Store each animation as they occur in the sort.
  // An animation is a record of old indices and index delta.
  // allAnimations is an array of arrays of animations.
  const allAnimations = []

  function g(X1,X2){
    function setLocalIndex(Xi, arrayIndex, c){ Xi.localIndex = arrayIndex + c }
    const gAnimations = []
    // console.log(`g(f([${X1.map(x => x.value)}]),f([${X2.map(x => x.value)}]))`)
    const S1 = f(X1)
    const S2 = f(X2)
    // console.log(`g([${S1.map(x => x.value)}],[${S2.map(x => x.value)}])`)
    // Set local indices, we will use them to calculate how far the 
    // line moves as a result of this step in the sort.
    // Every element of S2 is located after all of S1
    S1.forEach((s, i_) => setLocalIndex(s,i_,0))
    S2.forEach((s, i_) => setLocalIndex(s,i_,S1.length))

    const Y = []
    while(S1.length + S2.length > 0){
      let x = null
           if(S1.length === 0){ x = S2.shift() }
      else if(S2.length === 0){ x = S1.shift() }
      else if(S1[0].value < S2[0].value){   x = S1.shift() }
      else {                    x = S2.shift() }

      // We will record all the line swaps, in the order they occur
      // x's old index is just x.i
      // the delta is Y.length - localIndex
      x.d = Y.length - x.localIndex
      gAnimations.push({i: x.i, d: x.d })

      Y.push(x)
    }

    // Add this iterations animations to the list
    allAnimations.push(gAnimations)

    // We also need to update all the i's with their deltas
    Y.forEach(function(Yi){ Yi.i = Yi.i + Yi.d })

    return Y
  }

  function f(X){
    if(X.length === 1){ return X }
    const pivot = Math.ceil(X.length / 2)
    return g(X.slice(0,pivot), X.slice(pivot , X.length))
  }

  // Run the merge sort
  let ignoreTheResult = f(X)
  return allAnimations
}

// Animation which moves all lines with an entry in deltas by the specified d amount
function mergeAnimation(lines, deltas){
  // slow down when there are more lines to move
  const nFrames = 25 + deltas.length
  const waitFrames = 5
  const mapIndexToDelta = deltas.map(function(delta){ return { i: delta.i, d: delta.d * LINE_WIDTH / nFrames } })
  const mapIndexToIndexDelta = deltas.map(function(delta){ return { i: delta.i, d: delta.d } })

  let frameCount = nFrames + waitFrames
  // We will simultaneously move every line with a delta d lines
  return {
    done: () => frameCount === 0,
    run: function(){
      if(frameCount > waitFrames){
        lines.forEach(l => l.marked = false)
        mapIndexToDelta.forEach(function(id){
          const i = id.i
          const d = id.d
          lines[i].x += d
          lines[i].marked = true
        })
      }

      frameCount--

      if(this.done()){
        // Move in memory
        const newLines = lines.slice()
        mapIndexToIndexDelta.forEach(function(id){
          const replaceAt = id.i + id.d
          newLines[replaceAt] = lines[id.i]
        })
        newLines.forEach(function(nl, i_){ lines[i_] = newLines[i_] })
      }
    }
  }
}

function mergeSortAlgo(lines){

  const animationsFinished = () => animations != null && animations.length == 0

  let animations = null
  let current = null

  return {
    activeIndices: [],
    hasNext: () => false,
    getNext: () => null,
    done: () => current == null && animationsFinished(),
    run: function(){
      if(animations == null){
      const toSort = lines.map(function(l, i_){ return {i: i_, value: l.value} })
      animations = generateMergeSortAnimations(toSort)
      } else {
        if(this.done()) {
          return
        }
        // No current animation, get the next (if there is one)
        if(current == null){
          current = mergeAnimation(lines, animations.shift())
        } 

        // No else! current may have been set in the if.
        
        if(current != null) {
          // Run a frame of the current animation
          current.run()
          // If the current animation is done, set current to null for next iteration
          if(current.done()){
            current = null
          }
        }

      }
    }
  }
}

////////////////
// Quick Sort //
////////////////

// We can borrow mergeAnimation from merge sort, we just need to generate all the mergeAnimations to perform

function generateQuickSortAnimations(X_){
  // Same method as generateMergeSortAnimations - we'll run the sort, building up and array
  // of animations as we go.
  const allAnimations = []

  function quickSort(X, xStartIndex){
    if(X.length === 0) return X
    // For the animation we also need to keep track of the first element's index within
    // the entire array of lines - this is xStartIndex.

    // The first element will be our pivot
    const pivot = X[0]


    // left contains all those with values less than or equal to pivot's
    const left  = X.filter((x, i) => i !== 0 && x.value <= pivot.value)
    // right contains all those with values greater than pivot's
    const right = X.filter((x, i) => i !== 0 && x.value > pivot.value)

    // Now all elements of left are moved to the left of pivot
    // and all right are moved to its right
    const newX = left.concat([pivot]).concat(right)

    // Animate all these guys changing position. Deltas will be  new index - i.
    const animations = newX.map(function(x, newIndex){ return { i:x.i, d: newIndex - x.i + xStartIndex } })
    // Don't bother pushing it if its just a single element moving 0
    // if(animations.length > 1 || animations[0].d !== 0){
      allAnimations.push(animations)
    // }

    // Before sorting the next batches, we need to update their i's to be the new positions
    for (let i = 0; i < newX.length; i++) {
      newX[i].i = i + xStartIndex
    }

    // Now in newX all elements left of the pivot are lower value
    const nextLeft = newX.slice(0, left.length)
    const nextRight = newX.slice(left.length + 1)

    // Sort the left
    quickSort(nextLeft, xStartIndex)
    // Sort the right (track that the array is actually further to the right than it thinks)
    quickSort(nextRight, xStartIndex + 1 + nextLeft.length)
  }

  quickSort(X_, 0)
  return allAnimations
}

function quickSortAlgo(lines){

  const animationsFinished = () => animations != null && animations.length == 0

  let animations = null
  let current = null

  return {
    activeIndices: [],
    hasNext: () => false,
    getNext: () => null,
    done: () => current == null && animationsFinished(),
    run: function(){
      if(animations == null){
      const toSort = lines.map(function(l, i_){ return {i: i_, value: l.value} })
      animations = generateQuickSortAnimations(toSort)
      } else {
        if(this.done()) {
          return
        }
        // No current animation, get the next (if there is one)
        if(current == null){
          current = mergeAnimation(lines, animations.shift())
        } 

        // No else! current may have been set in the if.
        
        if(current != null) {
          // Run a frame of the current animation
          current.run()
          // If the current animation is done, set current to null for next iteration
          if(current.done()){
            current = null
          }
        }

      }
    }
  }
}

////////////
// Engine //
////////////

function engine(canvas, ctx, lines, algoGenerator, interactive){
  let animation = algoGenerator(lines)
  // Initial render (before starting)
  render(canvas, ctx, lines)

  // Setup onclick
  let started = false
  let finished = false
  canvas.onclick = () => started = true
  
  return {
    interactive: interactive,
    started: () => started,
    finished: () => false,
    run: function(){
      // Any animations running?
      if(!animation.done()){
        // Run the animation
        animation.run()
      }
      // If it's finished, start the next animation
      if(animation.done() && animation.hasNext()){
        if(animation.hasNext()){
          animation = animation.getNext()
        } else {
          finished = true
          console.log("finished")
        }
      }

    },
    render: function(){ 
      // Render inactive first
      render(canvas, ctx, lines.filter((l, i) => !animation.activeIndices.includes(i)))
      // Render active animations on top
      render_(ctx, lines.filter((l, i) => animation.activeIndices.includes(i)))
    }
  }
}

function start(engines){
  function renderOnce(timeStamp){
    window.requestAnimationFrame(renderOnce)
    // Run any engine which has been started
    const active = engines.filter(e => e.started() && !e.finished())
    active.forEach(e => e.run())
    // Render all canvases with a running engine, and any interactive canvases
    const renderable = engines.filter(e => (e.started() && !e.finished()) || e.interactive)
    renderable.forEach(e => e.render())
  }
  // Start running animations
  window.requestAnimationFrame(renderOnce)
}

///////////
// Setup //
///////////

// Sliders

const sliderVals = {}

Array.from(document.getElementsByClassName("colour-slider"))
  .forEach(function(element){
    sliderVals[element.id] = element.value
    element.oninput = function(){
      sliderVals[this.id] = this.value
    }
  })

// Initialise canvases and register click handlers
function hasClass(element, cls){ return element.classList.contains(cls) }

// Classes to pick an algorithm
function chooseAlgo(element){
  if(hasClass(element, "shuffle")){   return l => shuffleAlgo(l,0) }
  if(hasClass(element, "basicSort")){ return l => basicSortAlgo(l,1) }
  if(hasClass(element, "merge")){     return l => mergeSortAlgo(l) }
  if(hasClass(element, "bubble")){    return l => bubbleSortAlgo(l) }
  if(hasClass(element, "quick")){     return l => quickSortAlgo(l) }
}

// Classes to pick line generator
function chooseLineGen(element){
  if(hasClass(element, "debugLine")){ return x => debugLine(x*LINE_WIDTH)}
  if(hasClass(element, "colourOrd")){ return x => colourfulLine(x*LINE_WIDTH) }
  if(hasClass(element, "greyOrd")){   return x => greyscaleOrdLine(x*LINE_WIDTH) }
  if(hasClass(element, "grey")){      return x => greyscaleLine(x*LINE_WIDTH) }
  if(hasClass(element, "red")){       return x => vibrantLine(x*LINE_WIDTH, function(r,g,b){ return r }) }
  if(hasClass(element, "redHalf")){   return x => vibrantHalfRedLine(x*LINE_WIDTH)}
  if(hasClass(element, "random")){    return x => vibrantLine(x*LINE_WIDTH) }
  if(hasClass(element, "leastGrey")){ return x => leastGreyLine(x*LINE_WIDTH) }
  if(hasClass(element, "vibrantHue")){        return x => vibrantLine(x*LINE_WIDTH, function(r,g,b){ return hsl([r,g,b]).h }) }
  if(hasClass(element, "vibrantSaturation")){ return x => vibrantLine(x*LINE_WIDTH, function(r,g,b){ return hsl([r,g,b]).s }) }
  if(hasClass(element, "vibrantLightness")){  return x => vibrantLine(x*LINE_WIDTH, function(r,g,b){ return hsl([r,g,b]).l }) }
  if(hasClass(element, "randomHSL")){         return x => hslLine(x*LINE_WIDTH, function(h,s,l){ return h }) }
  if(hasClass(element, "randomH")){           return x => hLine(x*LINE_WIDTH, function(h,s,l){ return h }) }
  if(hasClass(element, "randomHlimitedSL")){  return x => hLimitLine(x*LINE_WIDTH, function(h,s,l){ return h }) }
  if(hasClass(element, "rainbowHSL")){        return x => rainbowLine(x*LINE_WIDTH) }
  if(hasClass(element, "cheatRainbowLine")){  return x => cheatRainbowLine(x*LINE_WIDTH) }
  if(hasClass(element, "bonusRainbowLine")){  return x => bonusRainbowLine(x*LINE_WIDTH) }
}


// Extract n from 100Lines, 10Lines, etc... class
function nLinesForCanvas(element){
  const nLinesRegex = /(\d+)Lines/
  let n = 100
  const nLinesClass = Array.from(element.classList).map(cls => cls.match(nLinesRegex)).filter(c => c !== null)[0]
  return parseInt(nLinesClass[1])
}

// Create engines
const engines = Array.from(document.getElementsByClassName("lines"))
  .map(element => {
    const ctx = element.getContext("2d")
    let interactive = false
    if(hasClass(element, "interactive")){
      interactive = true
    }
    return engine(element, ctx, range(nLinesForCanvas(element)).map(chooseLineGen(element)), chooseAlgo(element), interactive)
  })

// Begin rendering
start(engines)
