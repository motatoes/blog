function u(x){
  if(x === null || typeof x === "undefined") return true
  return false
}
function generate(n, f, acc){
  if(u(f)) f = x => x
  if(u(acc)) acc = 0
  const arr = new Array(n)
  for (let i = 0; i < n; i++) {
    const temp = f(i,n,acc)
    arr[i] = temp
    init = temp
  }
  return arr
}
function range(start, end){
  const n = Math.abs(start - end) + 1
  const f = i => start + (end > start ? 1 : -1) * i
  return generate(n, f)
}
function sum(arr){ return arr.reduce((x,y) => x + y, 0) }

const pi = Math.PI
const e = Math.E
const sin = Math.sin
const cos = Math.cos

// Data should be values <- [-1,1]
const data = generate(100, (x, n) => 0.9 * sin(pi * 2 * x / n))
// const data = generate(12, (x, n) => 0)
data.forEach(function(d_){if(d_ < -1 || d_ > 1){throw new Exception("Data out of bounds")}})
const N = data.length
const re = new Array(N)
const im = new Array(N)
const dataIm = new Array(N)

// Drawing modes - either draw on the input or output
const INPUT = "input"
const OUTPUT = "output"
let drawing = INPUT

// Change data - useful to call from console.
function change(f){
  for (let i = 0; i < N; i++) { 
    data[i] = f(i)
   }
}

function zero(){ change(() => 0) }

/////////////////////
// Transformations //
/////////////////////

function dataToYPix(d, h){
  // d <- [-1,1]
  // y <- (h,0]
  return h - (h / 2) * (d + 1)
}

function yPixToData(yPix, h){
  // yPix <- (h,0]
  // d <- [-1,1]
  return 1 - (2 * yPix) / h
}

function iToXPix(i,w){
  // i <- [0,N)
  // xPix <- [s / 2, w - (s/2))
  return (w + 2 * i * w)/(2 * N)
}

function xPixToI(xPix,w){
  let i = Math.floor((N * xPix) / w)
  // can go out of bounds due to borders around canvases (I think??)
  if(i < 0) i = 0
  if(i >= N) i = N - 1
  return i
}

// Set values in re and im global vars
function slowFourierTransform(data){
  // Real part
  data
    .map((x_, k) => sum(data.map((xi, n) => xi * (cos(2*pi*k*n/N)))))
    .forEach((x, i) => re[i] = x)
  // Imaginary part
  data
    .map((x_, k) => sum(data.map((xi, n) => xi * (-sin(2*pi*k*n/N)))))
    .forEach((x, i) => im[i] = x)
}

function inverseSlowFourierTransform(re, im){
  // Real part
  re
  .map((ignored, n) => (sum(re.map((ak, k) => ak * cos(2*pi*k*n/N)))
                     - sum(im.map((bk, k) => bk * sin(2*pi*k*n/N))))
                     /N)
  .forEach((x, i) => data[i] = x)

  // Imaginary part
  im
  .map((ignored, n) => (sum(re.map((ak, k) => ak * sin(2*pi*k*n/N)))
                     + sum(im.map((bk, k) => bk * cos(2*pi*k*n/N))))
                     /N)
  .forEach((x, i) => dataIm[i] = x)
}



// Initialise re and im
// console.log("data: " + data)
// console.log("dataIm: " + dataIm)
slowFourierTransform(data)
// console.log("re: " + re)
// console.log("im: " + im)
// inverseSlowFourierTransform(re, im)
// console.log("data: " + data)
// console.log("dataIm: " + dataIm)

///////////////
// Rendering //
///////////////

// Square side length s, centred on (x,y)
function squareCentre(ctx, x, y, s){
  const y_ = y - s/2
  const x_ = x - s/2
  ctx.fillRect(x_, y_, s, s)
  ctx.strokeRect(x_, y_, s, s)
}

function xAxis(ctx,w,h){
  ctx.strokeRect(0,h/2,w,0)
}

// Vertical line from (x,h/2) to (x,y)
function verticalLineCentre(ctx,x,y,h){
  const halfY = h/2
  ctx.strokeRect(x, halfY, 0, y - halfY)
}

function renderInput(ctx,props,fps){
  // Clear
  ctx.clearRect(0, 0, props.w, props.h)
  // Draw xAxis
  xAxis(ctx,props.w,props.h)
  // Draw data
  data.forEach((d,i) => {
    const xPix = iToXPix(i, props.w)
    const yPix = dataToYPix(d, props.h)
    verticalLineCentre(ctx, xPix, yPix, props.h)
    squareCentre(      ctx, xPix, yPix, props.s)
  })
  
  if(props.showFps){
    ctx.fillText(fps, 10, 10)
  }
}

function renderOutput(ctx,props,fps){
  // Clear
  ctx.clearRect(0, 0, props.w, props.h)
  // Draw xAxis
  xAxis(ctx,props.w,props.h)
  // Draw data

  // Real
  ctx.strokeWidth = 3
  ctx.fillStyle = "rgb(100,100,100)"
  ctx.strokeStyle = "rgb(30,30,30)"
  re.forEach((d,i) => {
    const xPix = iToXPix(i, props.w)
    const yPix = dataToYPix(d, props.h)
    verticalLineCentre(ctx, xPix, yPix, props.h)
    squareCentre(      ctx, xPix, yPix, props.s)
  })

  // Imaginary
  ctx.strokeWidth = props.strokeWidth
  ctx.fillStyle = props.squareFillColour
  ctx.strokeStyle = "rgb(30,100,30)"
  // ctx.strokeStyle = props.squareStrokeColour
  // ctx.strokeStyle = props.lineStrokeColour

  im.forEach((d,i) => {
    const xPix = iToXPix(i, props.w)
    const yPix = dataToYPix(d, props.h)
    verticalLineCentre(ctx, xPix, yPix, props.h)
    squareCentre(      ctx, xPix, yPix, props.s)
  })

  if(props.showFps){
    ctx.fillText(fps, 10, 10)
  }
}

////////////////////
// Parse Elements //
////////////////////

function hasClass(element, cls){ return element.classList.contains(cls) }

function addDrawHandler(element,w,h,mouse){
  const isInput = hasClass(element, INPUT)
  function isMouse2(e){
    return e.button !== 0 || e.shiftKey
  }
  function set(i, v, mouse2){
    if(isInput){
      drawing = INPUT
      data[i] = v
    } else {
      drawing = OUTPUT
      if(mouse2){
        im[i] = v
      } else {
        re[i] = v
      }
    }
  }
  element.onmousedown = function(e){
    mouse.drawing = true
    const x = e.offsetX
    const y = e.offsetY
    set(xPixToI(x,w), yPixToData(y, h), isMouse2(e))
    mouse.x = null
    mouse.y = null
  }
  element.onmouseup = function(e){
    mouse.drawing = false
    const x = e.offsetX
    const y = e.offsetY
    set(xPixToI(x,w), yPixToData(y, h), isMouse2(e))
    mouse.x = null
    mouse.y = null
  }
  element.onmousemove = function(e){
    if(!mouse.drawing) return;
    const x2 = e.offsetX
    const y2 = e.offsetY
    // First mouse move - don't crash
    if(mouse.x === null) mouse.x = x2
    if(mouse.y === null) mouse.y = y2

    // Linear interpolate to draw a line of points between (x1,y1) (x2,y2)
    const x1 = mouse.x
    const y1 = mouse.y

    // First and last x index
    const xi1 = xPixToI(x1,w)
    const xi2 = xPixToI(x2,w)
    // Transform start and end y vals, interpolate the transformed values
    const yt1 = yPixToData(y1, h)
    const yt2 = yPixToData(y2, h)
    const m = (xi1 === xi2) ? 0 : (yt2 - yt1) / (xi2 - xi1)
    const c = yt1 - (m * xi1)
    const interpolate = xi => m * xi + c
    range(xi1,xi2).forEach(xi => set(xi, interpolate(xi), isMouse2(e)))

    // Update mouse memory
    mouse.x = x2
    mouse.y = y2
  }
  element.oncontextmenu = () => false;
}

function buildEngines(){

  function getRenderer(element, ctx, props){
    if(hasClass(element, INPUT))  return function(fps){ return renderInput(ctx, props, fps)}
    if(hasClass(element, OUTPUT)) return function(fps){ return renderOutput(ctx, props, fps)}
  }


  function buildEngine(element){
    const ctx = element.getContext("2d")
    const props = {
      w: element.width,
      h: element.height,
      s: 6,
      strokeWidth: 1,
      squareFillColour: "rgb(30,213,54,0.3)",
      squareStrokeColour: "rgb(60,70,50,0.4)",
      lineStrokeColour: "rgb(30,20,40,0.8)",
      fpsFont: "16px serif",
      showFps: hasClass(element, "fps")
    }

    // Init context
    ctx.fillStyle = props.squareFillColour
    ctx.strokeStyle = props.squareStrokeColour
    ctx.font = props.fpsFont

    // Mouse memory object
    const mouse = {x:null,y:null}

    if(hasClass(element, INPUT) || hasClass(element, OUTPUT)){
      addDrawHandler(element, props.w, props.h, mouse)
    }

    return {
      element: element,
      ctx: ctx,
      props: props,
      // render: function(fps){ return renderInput(ctx, props, fps)}
      render: getRenderer(element, ctx, props)
    }
  }

  return Array
    .from(document.getElementsByClassName("engine"))
    .map(element => buildEngine(element))
}

function start(engines){
  let lastTimeStamp =  performance.now()
  function renderOnce(timeStamp){
    window.requestAnimationFrame(renderOnce)
    const fps = 1000/(timeStamp - lastTimeStamp)
    const fpsS = fps.toString().slice(0,4)
    lastTimeStamp = timeStamp

    if(drawing == INPUT){
      slowFourierTransform(data)
    } else {
      inverseSlowFourierTransform(re,im)
    }

    engines.forEach(e => e.render(fpsS))
  }
  // Start running animations
  window.requestAnimationFrame(renderOnce)
}

const engines = buildEngines()

start(engines)