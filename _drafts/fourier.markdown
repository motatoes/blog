---
layout: post
title:  "-"
# date:   2019-01-24 20:51:35 +0000
tags: []
---

<canvas id="testCanvas0" class="engine input fps" height="300px" width="800px"></canvas>
<canvas id="testCanvas2" class="engine output" height="300px" width="800px"></canvas>

<style>

canvas {
  cursor: pointer;
  padding-left: 0;
  padding-right: 0;
  margin-left: auto;
  margin-right: auto;
  display: block;
  border: solid
}

</style>

<script>
  {% include fourier.js %}
</script>